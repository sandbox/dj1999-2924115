<?php

namespace Drupal\domain_theme_switcher;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Class DomainThemeswitcherNegotiator.
 */
class DomainThemeswitcherNegotiator implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route = $route_match->getRouteObject();
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);
    return !$is_admin;
 }
 
  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $host = \Drupal::request()->getHost();
    return str_replace('.', '_', $host);
  }

}
